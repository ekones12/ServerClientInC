#include <stdio.h>
#include <stdlib.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */


int **createI2(char* argv,int row){
	FILE *f;
	int i=0 , k=1;
	int ch;
		int **matris = (int**)malloc(sizeof(int)*5*(row+1));
	 	int index;
	 	for (index=0;index<row+1;index++){	
	 	matris[index]=(int*)malloc(sizeof(int)*5);
		 }
	f=fopen(argv ,"r");
	if (!feof(f)){
	for( i=0;i<row+1;i++){
		ch = fgetc(f);
		if (ch=='A'||ch=='I'||ch=='S'){
			matris[i][0]=ch; 	
			fscanf(f, " %d %c\n",&matris[i][1],&matris[i][2]);; //printf("%c[C0][R%d]	%d[C1][R%d]	%c[C2][R%d]	\n",matris[i][0],i,matris[i][1],i,matris[i][2],i);	
		}
		else if (ch=='O'){
			matris[i][0]=ch;	
			fscanf(f, " %c %c\n",&matris[i][1],&matris[i][2]); //printf("%c[C0][R%d]	%c[C1][R%d]	%c[C2][R%d]	\n",matris[i][0],i,matris[i][1],i,matris[i][2],i);	
		}
		else{
			fscanf(f,"%d\n",&matris[i][0]);
			if(ch=='\n')
			i=0;
			}
		}	
	}
		fclose(f);
		return matris;
}


int **createI1(char* argv,int row){
	FILE *f;
	int i;
	int ch;
		f=fopen(argv, "r");
		int **matris = (int**)malloc(sizeof(int)*3*(row+1));
	 	int index;
	 	for (index=0;index<row+1;index++){	
	 	matris[index]=(int*)malloc(sizeof(int)*3);
		 }
	f=fopen(argv ,"r");
	if (!feof(f)){
	for( i=0;i<row+1;i++){
		ch = fgetc(f);
		fseek(f,-1,1);
		if(i==0){
		fscanf(f,"%d\n",&matris[i][0]);
		}
		else{
		fscanf(f, "%d %d\n",&matris[i][0],&matris[i][1]); //printf("%d[C0][R%d] %d[C1][R%d]\n",matris[i][0],i,matris[i][1],i);	
			}
		}
	}
	fclose(f);
	return matris;
}


int rownumber(char* argv){
	FILE *f; 
    int a, i = 0, j = 0; 
    f = fopen(argv, "r"); 
    if(!feof(f)){
    fscanf(f, "%d", &a);
    return a;
	}
	  fclose(f);
}
typedef struct {
	int top;
	int clientid;
	int maxsize;
	char *array;
	int serverid;
	char *history;
	char *serverhistory;
	int historyindex;
	int serverhistoryindex;
}stack;
typedef struct {
  int serverid;
  char *array;
  int first;
  int last;
  int clientid;
  int maxsize;
}queue;


stack *tanim(int clientid,int maxsize,int serverid){
	stack *s =(stack *)malloc(sizeof(stack)*maxsize);
	s->array=NULL;
	s->history=NULL;
	s->serverhistory=NULL;
	s->top=0;
	s->historyindex=0;
	s->serverhistoryindex=0;
	s->clientid=clientid;
	s->maxsize=maxsize;
	s->serverid=serverid;
	return s;
}

queue *tanimq(int clientid,int maxsize,int serverid){
	queue *q =(queue *)malloc(sizeof(queue));
	q->array=NULL;
	q->first=0;
	q->last=0;
	q->clientid=clientid;
	q->maxsize=maxsize;
	q->serverid=serverid;
	return q;
}

char deque(queue *q){
	if(q->first!=q->last){
		return q->array[q->first++];
		return ',';
	}
	else{	
	return ',';
	   }
}

int enque(char data,queue *q){
	if(q->array==NULL)
		q->array=(char*)malloc(sizeof(char)*q->maxsize*10);
	if((q->last+1)%(q->maxsize+1)!=q->first%(q->maxsize+1)){
		q->array[q->last++]=data;
		return 2;
	}
	else{
		return 1;
	}
}

char pop(stack *s){
	if(s->top>0){
	return s->array[--s->top];
	     }
	else{
	return '.';
}
}
int push(char data,stack *s){
	if(s->array==NULL)
	s->array=(char*)malloc(sizeof(char)*s->maxsize*10);
	if(s->top<s->maxsize){
		s->array[s->top++]=data;
		return 2;
	}
	else{
	return 1;
	}
}
void addclienthistory(char data,stack *s){
	if(s->history==NULL)
	s->history=(char*)malloc(sizeof(char)*1000);
	s->history[s->historyindex++]=data;
	}
	
void addserverhistory(char data,stack *s){
	if(s->serverhistory==NULL)
	s->serverhistory=(char*)malloc(sizeof(char)*1000);	
	s->serverhistory[s->serverhistoryindex++]=data;
	}
	
void writeoutput(stack **s,int row,char *argv){
	
		FILE *output;
		output = fopen(argv,"w");
		
		int j = 1 ;
		
		while(j<row){	
		
			int i = 0 ;
			while(i<s[j]->historyindex){
				putc(s[j]->history[i],output);
				if (i+1!=s[j]->historyindex)
				putc(' ',output);
				printf("%c ",s[j]->history[i]);
				i++;
				}
			printf("<----client:%d\n",j);
			putc('\n',output);
			j++;
	}
	int i =0;
	while(i<s[row]->serverhistoryindex){	
	putc(s[row]->serverhistory[i],output);
	if (i+1!=s[row]->serverhistoryindex)
	putc(' ',output);
	printf("%c ",s[row]->serverhistory[i]);
		i++;	
	}printf("<----server\n"); 
    fclose(output);
	}

stack **setstack(int rownumber,int **matrix){
	stack **s1=(stack**)malloc(sizeof(stack)*800);
	int i=0;
	for(i=1;i<rownumber;i++){
		s1[i]=(stack *)malloc(sizeof(stack)*5);
	}
	
	for(i=1;i<rownumber;i++){
		s1[i]=tanim(i,matrix[i][1],rownumber);
}
	s1[rownumber]=tanim(-1,matrix[rownumber][1],rownumber);
	return s1;
}
queue **setqueue(int rownumber,int **matrix){
	queue **q1=(queue**)malloc(sizeof(queue)*800);
	int i=0;
		for(i=1;i<rownumber;i++){
		q1[i]=(queue *)malloc(sizeof(queue)*5);
	}
	for(i=1;i<rownumber;i++){
		q1[i]=tanimq(i,matrix[i][0],rownumber);
	}
	q1[rownumber]=tanimq(-1,matrix[rownumber][0],rownumber);
	return q1;
}


void operates(int **matris,int server ,int row,stack **mystacks,queue **myqueues){
	int i,b;
	char a;
	for (i=1;i<row+1;i++){
		char operand= matris[i][0];
		switch(operand){
			case('A'): //komuttaki client id sinin quesine komuttaki harf atanacak ;
				if (enque(matris[i][2],myqueues[matris[i][1]])==1){//que fullse client historye 1 yaz�lacak ; printf("Queue is full,client history data updaated\n");
					addclienthistory('1',mystacks[matris[i][1]]);  //que fullse client historye 1 yaz�lacak ;
				}	//komuttaki client id sinin quesine komuttaki harf atanacak ;
				break;
			case('I'):
					if (push(matris[i][2],mystacks[matris[i][1]])==1){	// I gelince stackler fullse o stackin historysine 2 yaz�lacak; printf("stack is full,client history data updaated\n"); printf("server %d\n",server); printf("matris %d\n",matris[i][1]);
					if ( matris[i][1] == server){//printf("control 1\n");					
					addserverhistory('2',mystacks[server]);
				}
					else{
					addclienthistory('2',mystacks[matris[i][1]]);
					}
				}
				break;
			case('S'):
				a = pop(mystacks[matris[i][1]]);
				if(a == '.'){
						char b;
						b = deque(myqueues[matris[i][1]]);
						if(b == ','){ // verilen idnin stack ve quesi aynanda bo�sa ve at�lcak eleman yoksa client history 3 yaz�lacak;
						addclienthistory('3',mystacks[matris[i][1]]);
						break;
						}
						else{ // komuttaki cliend id sinin stackindeki sonras�ndan quesindeki itemler serverin quesine atanacak;
						if (enque(b,myqueues[myqueues[matris[i][1]]->serverid]) == 1){
						addserverhistory('1',mystacks[server]);
						}
						addclienthistory(b,mystacks[matris[i][1]]);
						}
				}
				else{	// komuttaki cliend id sinin stackindeki sonras�ndan quesindeki itemler serverin quesine atanacak;
				if (enque(a,myqueues[server])==1)
				addserverhistory('1',mystacks[server]);	
				addclienthistory(a,mystacks[matris[i][1]]);
				}
				break;
				
			case('O'):	a = pop(mystacks[server]);
					if(a == '.'){
						char b;
						b = deque(myqueues[server]);
						if(b == ','){
						addserverhistory('3',mystacks[server]);
						break;
						}
						else
						addserverhistory(b,mystacks[server]);
				}
				else
				addserverhistory(a,mystacks[server]);
				break;	
		}
	}
}
int main(int argc, char *argv[]) {	
	int** mDizi;
	int** mDizi2;
	
	stack **mystacks;
	queue **myqueues;
	
	int rownumber1= rownumber(argv[1]);
	int rownumber2= rownumber(argv[2]);
	
	mDizi =createI1(argv[1],rownumber1);
	printf("\n");
	
	mDizi2=createI2(argv[2],rownumber2);
	printf("\n");
	
	
	mystacks=setstack(rownumber1,mDizi);
	myqueues=setqueue(rownumber1,mDizi);

	operates(mDizi2,rownumber1,rownumber2,mystacks,myqueues);
	writeoutput(mystacks,rownumber1,argv[3]);
	
	return 0;
}
